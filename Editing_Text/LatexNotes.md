# LaTex, The only way to write documents

LaTex has a wide variety of uses, however to put it quite broadly it's used for documents of many kinds (scientific, doumentation, reasearch papers, etc). I'm writing this as I've adopted this as my main for of transcribing my personal writings to convert to pdf.
I'm going to be going over very basic syntax and such.

The way the syntax works in LaTex is like this `\variable{parameters`

There's even some cases where you can next those variables liek this `\variable{Some text here \thanks{thank you billard}}`

Example of a bare minimun setup of a document
```
\documentclass{article}
 
\begin{document}

First document. This is a simple example, with no 
extra parameters or packages included.

\end{document}
```

Notice that this doesn't have a title, if you want to add it then you need to add the \variables and then cause that to render with \maketitle

Here's an example of it use
```
\documentclass[12pt, letterpaper, twoside]{article}
\usepackage[utf8]{inputenc}
 
\title{First document}
\author{Hubert Farnsworth \thanks{funded by the Overleaf team}}
\date{February 2017}

\begin{document}
 
\maketitle
 
We have now added a title, author and date to our first \LaTeX{} document!
 
\end{document}
```

You can add comments much like in programming languages, you just need to do it like so `% Your comment here`

Here are just a few text styling variables that you can use in your work
```
\textbf{Bold}
\underline{Underlined} 
\textbf{\textit{Bold then Italic}}
\emph{emphasis} 
```

Example of adding an image: 
```
\documentclass{article}
\usepackage{graphicx}
\graphicspath{ {images/} }
 
\begin{document}
The universe is immense and it seems to be homogeneous, 
in a large scale, everywhere we look at.
 
\includegraphics{universe}
 
There's a picture of a galaxy above
\end{document}
```
You'll notice that you have to include the graphicx package, and that due to LaTex not natively supporting images, but if you downloaded the full package of TexLive on your machine then it should be recognized.

Here's an example of adding an image, adding styling and then adding a proper caption
```
\begin{figure}[h]
    \centering
    \includegraphics[width=0.25\textwidth]{mesh}
    \caption{a nice plot}
    \label{fig:mesh1}
\end{figure}
 
As you can see in the figure \ref{fig:mesh1}, the 
function grows near 0. Also, in the page \pageref{fig:mesh1} 
is the same example.
```

Here's a an ordered and unordered list
```
\begin{enumerate}
  \item This is the first entry in our list
  \item The list numbers increase with each entry we add
\end{enumerate}


\begin{itemize}
  \item The individual entries are indicated with a black dot, a so-called bullet.
  \item The text in the entries may be of any length.
\end{itemize}
```

These are pretty much the basics to writing in LaTex, everything else that you may need you can reference else where, this is mainly to wet your pallete and see the syntax for common things.
