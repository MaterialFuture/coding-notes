# CSS Notes


## Basics

So CSS is pretty easy to go over, it’s one of those languages that’s easy to learn and hard to master.

CSS and HTML go hand in hand, so if you know HTML, then styling it will be pretty easy, it’s just a few syntax things you need to get used to.

### Structure


So the structure for a block of CSS looks like this;

```
Selector {
    property: value, boolean, etc;
    ...
}
```

So this might look familiar to you if you’ve ever inspected a web page before. Any element on the page can be selected and changed with this structure. Let me explain better what all these parts of the structure do before I go any further.


### Syntax

CSS is pretty straight forward, if you’ve ever worked with any Languages like Javascript or Python then it’ll look very familiar.

So first off, the Selector. This is where you’d select the HTML tag, ID, or class. Which each class theres different syntax for accessing that piece of content.
- For an HTML tag such as `<a>` or `<h3>`, you just type in the name of the tag. This selects all of those tags on the screen. These aren’t case sensitive but it’s still very important to make sure they’re lowercase.
- For an HTML class such as `<a class="button">` in css you’d select it by saying `.button`, the `.` Represents that it’s a class. This selects all tags with that css class. This is case sensitive, so please make sure it’s exactly how it is in the HTML file.
- And finally for an ID you’d select it with `#`, and that'll select all the IDs with that name. This is case sensitive, so please make sure it’s exactly how it is in the HTML file.

So now that you’ve selected something you’ll probably want to start editing the element itself. So outside of the selector put two curly brackets like so 

`#Selector {}`

Now you could just putting the css properties in there and start hacking away, but for both readability and for the sake of whoever else needs to work on your code, lets do something better. In between the brackets press enter twice to separate them out like so;

```
#Selector {

}
```

It should look like this now, which is right where you want to be.
So now this is three rows of code, in the middle of them is where all the magic in CSS happens. Let’s use a real example of a property and it’s values. In the second row, press tab, then type in lowercase `margin: 10px;`. This creates a 10px margin around the whole object you’ve selected. You can get more specific by putting `margin-left: 15px;` afterwards, or if you’re really confidant `margin: 10px 0 10px 0;` This adds margins to only the top and bottom in just one property.
So as you can see after margin there’s a `:` and after the value there’s `;`, these define where the value starts and ends, `:` being start, and `;` being end. Without these, you’re code from one selector could leak into another selector until it see the next `;` and so on. Your code should look like this now;

```
#Selector {
    margin: 10px;
}
```

### Note

So you CSS file is read from top to bottom, column by column, by your browser. So while this not only means you have to structure your file to how it would be read, but keep in mind of multiples of a class. So if I have a class that sets 20px of padding, but later on I select the same element and set the padding to 10px, the one thats read last will be the one you can see on the page.
This also goes for multiple files, so if you load in multiple CSS files, the later ones will overwrite multiples of the same element.


## Advanced

### Nesting Elements

So let’s say you want to select a specific class, but only inside an anchor tag <a>, or more specifically, something inside a div with a  class of “container” inside a <p> inside an <a> with the class of btn. This sounds specific but it’s very common that you’ll need to select specific classes. Most might say to put an ID on the element (which you should avoid doing), or put a specific class to it. But maybe you’re editing someone elses code and don’t have access to edit it. Let me show you how you can do this.

```
.container p a .btn {
    margin: 0 auto;
    background: blue;
}
```

This is how you can find more specific classes and not have your css leak into unwanted classes and elements

If you want to add the same property to multiple classes without having to create a code block each time, there’s an easy way to do that. You do what you’d do to select the element, then put a comma after that and select the other element you want to edit. Like this;

```
.container p a .btn, .container p ul li .list-item, #SomeID {
    margin: 10px;
    background: black;
    text-decoration: none;
}
```

This is useful for trying to also make the file size smaller if you’re worried about speed.


## More Advanced

### Media 

Media is for more responsive development, and is very useful. Let’s say you have an element that’s on the page just how you want it when the page is full screen but when you have the window at half size or try to resize the window it’s gone or broken, well you can fix that the media class. It takes time messing with your browsers dev tools to get where to start on something like this, but it can really help in the long run. A media selector should look something like this.

```
@media screen and (max-width: 1200px;){
    .SomeClass {
        width: 500px;
    }
}
```

So this makes sure that `.SomeClass` at a screen width of 1200px will change the width of that element to 500px. You’ll also notice that there’s a CSS block within another CSS block, this because you can nest blocks within a media selector, and it’s unique to that selector. And with this you can change the properties of whatever you want at whatever screen size.


So that’s the basics of CSS essentially. There’s so many different selectors, I’d reccmend reading on the W3 site whichever ones are relevant to what you’re working with bevause they all have their own specific values that they handle, like `auto`, `true`, `none`, `inherit`, etc...

If you want to look into SASS (which is a languages that compiles to CSS and has similar syntax but has more features), you can see how to nest blocks of css inside one another so it compiles into regular nested css like above. It saves time but it’s always better to learn the basics.














