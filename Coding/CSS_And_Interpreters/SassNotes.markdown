# More mass with SASS

Sass is an extension of CSS that adds power and elegance to the basic language. It allows you to use variables, nested rules, mixins, inline imports, and more, all with a fully CSS-compatible syntax. Sass helps keep large stylesheets well-organized, and get small stylesheets up and running quickly, particularly with the help of the Compass style library.

Features
- Fully CSS-compatible
- Language extensions such as variables, nesting, and mixins
- Many useful functions for manipulating colors and other values
- Advanced features like control directives for libraries
- Well-formatted, customizable output


If you’re running a bash or zsh terminal make sure you have the sass gem installed, although for this doc I’m going to go over Compass, a tool that compiles sass in the terminal, you can get Compass by installing the Gem. If you don’t already have Ruby installed I’d recommend it because there are some really great developer tools that can be installed with it. Refer here if you don’t have Ruby installed in the terminal.

If it’s your first time setting up a Gem then lets prepare the install;
Setting up the ruby environment
`sudo gem update --system`
`sudo gem install compass`
`sudo gem install compass --pre`

Now lets install Compass (I know it’s a lot of steps so far but it’s worth it if you want to use Ruby in the future)
`sudo gem install compass`


Now Compass can do a lot of things, for example it can watch your SCSS files to watch for changes, and then whenever there is it’ll compile it to css. If you know CSS then SASS will be a breeze. If you read my notes over CSS you’ll notice I outlined a lot of the positives to having SASS.

Heres an example for a bit of code for creating different styles for anchor tags;
```
button,
.button {

    line-height: line-height(xs);

    display: inline-block;
    min-height: input(height);
    padding: #{(input(height) - line-height(xs))/2} button(padding);
    vertical-align: middle;

    color: button(neutral-color);
    background-color: button(neutral-background);
    border: transparent input(border-width) solid;
    border-radius: input(border-radius);

    transition: border-color .1s linear;


    &:focus {
        border-color: input(border-color-focus) !important;
        outline: 0;
    }

    &:active {
        transform: translateY(1px);
    }

    &.-rounded {
        border-radius: input(height);
    }

    &.-stretched {
        width: 100%;
    }
}
.button--primary {
    @extend .button;

    color: button(primary-color);
    background-color: button(primary-background);


}
.button--secondary {
    @extend .button;

    color: button(secondary-color);
    background-color: button(secondary-background);


}
.button--success {
    @extend .button;

    color: button(success-color);
    background-color: button(success-background);

}
.button--warning {
    @extend .button;

    color: button(warning-color);
    background-color: button(warning-background);

}
.button--danger {
    @extend .button;
    color: button(danger-color);
    background-color: button(danger-background);

}
``` 

This will compile to css to look like;
```
button,
.button,
.button--primary,
.button--secondary,
.button--success,
.button--warning,
.button--danger {
  line-height: line-height(xs);
  display: inline-block;
  min-height: input(height);
  padding: input(height)-line-height(xs)/2 button(padding);
  vertical-align: middle;
  color: button(neutral-color);
  background-color: button(neutral-background);
  border: transparent input(border-width) solid;
  border-radius: input(border-radius);
  transition: border-color .1s linear;
}
button:focus,
.button:focus,
.button--primary:focus,
.button--secondary:focus,
.button--success:focus,
.button--warning:focus,
.button--danger:focus {
  border-color: input(border-color-focus) !important;
  outline: 0;
}
button:active,
.button:active,
.button--primary:active,
.button--secondary:active,
.button--success:active,
.button--warning:active,
.button--danger:active {
  transform: translateY(1px);
}
button.-rounded,
.button.-rounded,
.-rounded.button--primary,
.-rounded.button--secondary,
.-rounded.button--success,
.-rounded.button--warning,
.-rounded.button--danger {
  border-radius: input(height);
}
button.-stretched,
.button.-stretched,
.-stretched.button--primary,
.-stretched.button--secondary,
.-stretched.button--success,
.-stretched.button--warning,
.-stretched.button--danger {
  width: 100%;
}
.button--primary {
  color: button(primary-color);
  background-color: button(primary-background);
}
.button--secondary {
  color: button(secondary-color);
  background-color: button(secondary-background);
}
.button--success {
  color: button(success-color);
  background-color: button(success-background);
}
.button--warning {
  color: button(warning-color);
  background-color: button(warning-background);
}
.button--danger {
  color: button(danger-color);
  background-color: button(danger-background);
}
```
You’ll probably notice how the...


...

