# Simple Terminal (Bash) commands


## You Know The Simple Stuff

- `cd` - Change Directory
- `mkdir` - Make Directory
- `pwd` - Path of Working Directory
- `ls` - List

But there's more to these simple commands that you might not know.




### Remove a file or non-empty directory
rm -rf



### Create Empty File
touch <filenoame and extension>
