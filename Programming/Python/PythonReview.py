def hotel_cost(nights):
  return 140 * nights

def plane_ride_cost(city):
  if city == "Charlotte":
    return 183
  elif city == "Tampa":
    return 220
  elif city == "Pittsburgh":
    return 222
  elif city == "Los Angeles":
    return 475

def rental_car_cost(days):
  cost = days * 40
  if days >= 7:
    cost -= 50
  elif days >= 3:
    cost -= 20
  return cost

def trip_cost(city, days, spending_money):
  return rental_car_cost(days) + hotel_cost(days) + plane_ride_cost(city) + spending_money

print trip_cost("Los Angeles", 5, 600)


# Lists
zoo_animals = ["pangolin", "cassowary", "sloth", "jingu"];
if len(zoo_animals) > 3:
  print "The first animal at the zoo is the " + zoo_animals[0]
  print "The second animal at the zoo is the " + zoo_animals[1]
  print "The third animal at the zoo is the " + zoo_animals[2]
  print "The fourth animal at the zoo is the " + zoo_animals[3]

# Appending Items
suitcase = []
suitcase.append("sunglasses")
suitcase.append('bathing suit')
suitcase.append('pillow')
suitcase.append('teeth')
list_length = len(suitcase) # Set this to the length of suitcase
print "There are %d items in the suitcase." % (list_length)
print suitcase


# Slices
animals = "catdogfrog"
cat = animals[:3]
dog = animals[3:6]
frog = animals[6:]

# Insert and move around
animals = ["aardvark", "badger", "duck", "emu", "fennec fox"]
duck_index = animals.index("duck") # Use index() to find "duck"
animals.insert(duck_index, "cobra")
print animals # Observe what prints after the insert operation


# List out array
my_list = [1,9,3,8,5,7]
for number in my_list:
  print 2 * number
