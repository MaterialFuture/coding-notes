These notes are for the audit I ran on ExampleSite.com as of mid-January, as well as ways to optimize how files are output. I wrote these partially for me to follow and keep in mind when it comes down to maintaining and optimizing the site in the future. Keep in mind that the the site constantly changing and that the stats may not reflect how things will be in the future.

Initial: 
After the page is fully loaded (at ~xxx for DOM content, and ~xxx for it to be fully loaded. And xxx) The page has made roughly ~xxx+ HTTP Requests.

A well Optimized site should be under 100 requests. For example, A site like YouTube has around ~70 requests while being around ~1.5Mb.

Results below:
Null