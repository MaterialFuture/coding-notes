## Lesson 1

#### Lesson 1.1
After reviewing the materials above, including reference material and extraneous resources found elsewhere, I have found that 'Design Thinking' is a very humanist as well as empathetic approach to solving various issues with design. The Scientific Method comes to mind when researching 'Design Thinking'. The two are very similar in structure, but are in fact different from one another, as one is expanding and testing/iterating upon an idea, while the other relies on empathy, asking questions, and testing how those questions affect the process and asking more questions to then start iterating. 
The 'Design Thinking' method seems to be the most efficient way to solve problems in design by breaking down how humans interact with the world around them and figuring out ways to make their lives easier by testing using real world examples. The benefits of going this route would be that you're gathering real world data really understanding your users rather than just testing within a bias and creating what you personally think is the best solution. This takes the 'you' out of the equation and allows the 'user' to be the prime focus of the process, as it should be, as they would be the ones using your product.

#### Lesson 1.2
​A problem I've faced is the consumption of media or content. I feel as though there should be a more mindful approach to how we consume content as how we can take back our time. One problem I've noticed through family and friends is that people seem to be consumed by the content they want to consume, having to move around their schedule to watch the game, or to binge their favorite show. I feel as though there could be a more mindful way to monitor these activities and make time for the more important things in our lives.
My solution would be;
- For computers, logging the time active on screen.
- For Smart TVs, logging the amount of time spent watching something.
- For phones, logging screen time. (Much like how Apple introduced this into)
All this logged time could be viewable in one place, patterns learned, then reminders can be created for important tasks such as chores, working out, going for a walk, etc.
My concern is that people spend so much time on their devices, they might even log in their time, but even with that there's no action to be taken, no real strategy on what to do with that time spent and there is more room for excuses.

#### Lesson 1.3
​A major obstacle​ my the team currently is currently facing are the biases of current platforms like Amazon and Instagram for every example in the Design and process. 
Another obstacle would be 


#### Lesson 1.6
As of current, I’ve worked with three different companies where I used a form of Design Thinking to solve problems that users were facing.

One example of a problem we faced and solved using design thinking was during my time at a major retailer, the team received feedback from users over how they would like ads to be shown on the site for new items and the latest deals. We built storyboards, and designed different approaches. 
It came down to observing our competition and seeing what has been done, and how to modify and update that experience to better fit our brand. After a long night of coding away and hours of testing, I got a working version of it up that, despite the time constraint of the project, ended up being really profitable and helped garner almost 1 Million Clicks in the couple of days near the holidays, and a Half-Million the other time it was up for an event.

The quick turn around and use of design thinking in the planning stages really allowed the team to come up with something useful, relevant, and non-intrusive.

#### Lesson 2.1
I’m seeing more and more the benefits of using Design Thinking in both my everyday life and in my work life. An example of one way this type of thinking can benefit me in my projects would be when I’m working on a freelance project and a client comes to me with an idea they would like to create. I can use design thinking and user interviews to better create a mockup of the ideas presented to me from the client, and make something everyone can be happy with and enjoy using.
Another example of how I can benefit from design Thinking is how I can approach learning. For the most part I’ve used a variant of the Scientific method of learning something like coding (hypothesize, iterate, repeat), but I can now try and use a sort of design thinking method to better myself as a designer by understanding the purpose of what I am to create and better understand why I am creating to begin with. 


#### Extra
Design Thinking very much reflect Carl Jung’s writings of the cognitive processes and how he believed in human-centered design. In his book (Philemon) he talks about his processes and such with the focus being around being very human-centered. Inspiration, Ideation and Implementation were the core principals of this focus. Experimentation and the writing out (prototyping via sketching in the case of design-thinking) of processes to better iterate on them in testing was very apparent in his writings. He very much 










