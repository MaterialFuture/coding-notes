# Coding, Programming, and Notes

This is a repo backing up my notes and documents over different things I've worked on. I'm going to attempt to leave the olde notes I've writen here for the time being, but in the future I may put those soemwhere else

What is this trying to achieve? I want this to be open so others can learn if they want, but for the most part it's for myself to have a place to put notes and whatnot over the technologies I've learned or useful things I may refer to later.

If you're ever stuck on a command or want to know more about a command line tool, always remember `man <command>` will tell you everything you need to know. If man isn't available, try the Arch Wiki, and then lastly look up on the official online documentation.
